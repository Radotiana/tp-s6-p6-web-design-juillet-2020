create database foot;
use foot;

create table utilisateur(
    login varchar(5) not null,
    pwd varchar(150) not null
);

insert into utilisateur values ('admin','admin');

create table actualite(
    idActualite     int not null AUTO_INCREMENT,
    daty            datetime not null,
    titre           text not null,
    photo           varchar(70),
    etat            int,
    description     text not null,
    primary key(idActualite)
);

create table categorie(
    idCategorie     int not null  AUTO_INCREMENT,
    libele          varchar(20) not null,
    primary key(idCategorie)
);
insert into categorie values(1,'U-10');
insert into categorie values(2,'U-13');
insert into categorie values(3,'U-15');
insert into categorie values(4,'U-17');
insert into categorie values(5,'U-20');
insert into categorie values(6,'U-23');
insert into categorie values(7,'2eme division');
insert into categorie values(8,'1ere division');
insert into categorie values(9,'Veteran');
insert into categorie values(10,'Feminine');

create table competition(
    idCompetition   int not null AUTO_INCREMENT,
    idCategorie     int not null,
    libele          varchar(200) not null,
    logo            varchar(30) not null,
    nombreequipe    int not null,
    saison          varchar(9) not null,
    etat            int not null,
    primary key(idCompetition)
);

alter table competition add constraint FK_REFERENCE_1 foreign key (idCategorie)
      references categorie (idCategorie) on delete restrict on update restrict;

create table matchs(
    idMatch         int not null AUTO_INCREMENT,
    idCompetition   int not null,
    idCategorie     int not null,
    daty            datetime not null,
    stade           varchar(200) not null,
    domicile        int not null,
    scoreA          int not null,
    exterieur       int not null,
    scoreB          int not null,    
    etat            int not null,
    primary key(idMatch)
);
alter table matchs add constraint FK_REFERENCE_2 foreign key (idCompetition)
      references competition (idCompetition) on delete restrict on update restrict;
alter table matchs add constraint FK_REFERENCE_10 foreign key (idCategorie)
      references categorie (idCategorie) on delete restrict on update restrict;
/*alter table matchs add constraint FK_REFERENCE_11 foreign key (domicile)
      references equipe (idEquipe) on delete restrict on update restrict;
alter table matchs add constraint FK_REFERENCE_12 foreign key (exterieur)
      references equipe (idEquipe) on delete restrict on update restrict;*/

create table equipe(
    idEquipe        int not null AUTO_INCREMENT,
    idCategorie     int not null,
    nom             varchar(20) not null,
    logo            varchar(30) not null,
    region          varchar(30) not null,
    president       varchar(20) not null,
    coach           varchar(20) not null,
    primary key(idEquipe)
);
alter table equipe add constraint FK_REFERENCE_3 foreign key (idCategorie)
      references categorie (idCategorie) on delete restrict on update restrict;

create table classement(
    idClassement    int not null AUTO_INCREMENT,
    idCompetition   int not null,
    idCategorie     int not null,
    idEquipe        int not null,
    points          int not null,
    nbmatch         int not null,
    goalaverage     int not null,   
    primary key(idClassement)
);
alter table classement add constraint FK_REFERENCE_4 foreign key (idCompetition)
      references competition (idCompetition) on delete restrict on update restrict;
alter table classement add constraint FK_REFERENCE_5 foreign key (idEquipe)
      references equipe (idEquipe) on delete restrict on update restrict;
alter table classement add constraint FK_REFERENCE_9 foreign key (idCategorie)
      references categorie (idCategorie) on delete restrict on update restrict;

create table place(
    idPlace         int not null AUTO_INCREMENT,
    libele          varchar(20),
    primary key(idPlace)
);
insert into place values(1,'Gardien de but');
insert into place values(2,'Defenseur');
insert into place values(3,'Milieu de terrain');
insert into place values(4,'Attaquant');

create table joueur(
    idJoueur        int not null AUTO_INCREMENT,
    idEquipe        int not null,
    idCategorie     int not null,
    idPlace         int not null,
    nom             varchar(20) not null,
    prenom          varchar(20) not null,
    numero          int not null,
    photo           text not null,
    primary key(idJoueur)
);
alter table joueur add constraint FK_REFERENCE_6 foreign key (idEquipe)
      references equipe (idEquipe) on delete restrict on update restrict;
alter table joueur add constraint FK_REFERENCE_7 foreign key (idPlace)
      references place (idPlace) on delete restrict on update restrict;
alter table joueur add constraint FK_REFERENCE_8 foreign key (idCategorie)
      references categorie (idCategorie) on delete restrict on update restrict;

DROP TABLE IF EXISTS sequence;
CREATE TABLE sequence (
name              VARCHAR(50) NOT NULL,
current_value INT NOT NULL,
increment       INT NOT NULL DEFAULT 1,
PRIMARY KEY (name)
) ENGINE=InnoDB;
INSERT INTO sequence VALUES ('idEquipe',0,1);
DROP FUNCTION IF EXISTS currval;
DELIMITER $
CREATE FUNCTION currval (seq_name VARCHAR(50))
RETURNS INTEGER
CONTAINS SQL
BEGIN
  DECLARE value INTEGER;
  SET value = 0;
  SELECT current_value INTO value
  FROM sequence
  WHERE name = seq_name;
  RETURN value;
END$
DELIMITER ;


DROP FUNCTION IF EXISTS nextval;
DELIMITER $
CREATE FUNCTION nextval (seq_name VARCHAR(50))
RETURNS INTEGER
CONTAINS SQL
BEGIN
   UPDATE sequence
   SET          current_value = current_value + increment
   WHERE name = seq_name;
   RETURN currval(seq_name);
END$
DELIMITER ;
-- VIEWS:

create or replace view matche as(
    Select competition.libele,e1.region as region1,e2.region as region2,m.*,e1.logo as domiciles,e2.logo as exterieurs,e1.nom as domicil,e2.nom as exterieure From matchs m join equipe e1 on m.domicile=e1.idEquipe join equipe e2 
    on m.exterieur=e2.idEquipe join competition on m.idCompetition=competition.idCompetition
);


