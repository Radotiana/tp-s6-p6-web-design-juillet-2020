<!DOCTYPE html>
<html lang="en">

<head>
  <title>GasyFou't - le site web du foot Malagasy</title>
  <meta charset="utf-8">
  <meta name="description" content="GasyFou't est l'une des sites web qui parle entièrement le foot Malagasy">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?php echo base_url('/assets/fonts/icomoon/style.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/magnific-popup.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/jquery-ui.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.carousel.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.theme.default.min.css'); ?>">


  <link rel="stylesheet" href="<?php echo base_url('/assets/css/aos.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/style.css'); ?>">

</head>

<body>
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-logo">
          <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="<?php echo base_url('/images/logo.png'); ?>" alt="logo"></a>
        </div>
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar absolute transparent" role="banner">
      <nav class="site-navigation position-relative text-right bg-black text-md-right" role="navigation">
        <div class="container position-relative">
          <div class="site-logo">
            <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="<?php echo base_url('/images/logo.png'); ?>" alt="logo"></a>
          </div>

          <div class="d-inline-block d-md-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li>
              <h2><a href="#">Accueil</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - Actualités du foot Malagasy/actualite.php'); ?>">Actualités</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - Les matchs à venir - Calendrier/match-1.php'); ?>">Matchs</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - BAREA de Madagascar/equipe.php'); ?>">Equipes</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou \'t - Classement sur toutes les championnats/classement.php'); ?>">Classement</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou \'t - Résultats, Scores/resultat-1.php'); ?>">Résultats</a></h2>
            </li>
          </ul>
        </div>
      </nav>
    </header>



    <!-- <div class="slide-one-item home-slider owl-carousel"> -->
    <div class="site-blocks-cover overlay" style="background-image: url(<?php echo  base_url('/images/barea.jpg'); ?>);" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-start">
          <div class="col-md-6 text-center text-md-left" data-aos="fade-up">
            <h1 class="bg-text-line">GasyFou't - le site web du foot Malagasy</h1>
            <h4 style="color:white">GasyFou't est l'une des sites web qui parle entièrement le foot Malagasy</h4>
            <!-- <p><a href="#" class="btn btn-primary btn-sm rounded-0 py-3 px-5">Voir plus</a></p> -->
          </div>
        </div>
      </div>
    </div>
    <!-- </div> -->
    <div class="site-section pt-0 feature-blocks-1" data-aos="fade" data-aos-delay="100">
      <div class="container">
        <div class="row">
          <?php for ($i = 0; $i < count($une); $i++) { ?>
            <div class="col-md-6 col-lg-4">
              <div class="p-3 p-md-5 feature-block-1 mb-5 mb-lg-0 bg" style="background-image: url(https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $une[$i]->photo; ?>);height:280px">
                <div class="text">
                  <h2 class="h5 text-white"><?php echo $une[$i]->titre; ?></h2>
                  <p>Pour voir plus de détails, veuillez cliquer sur le bouton ci-dessous</p>
                  <p class="mb-0"><a href="<?php echo base_url($une[$i]->titre.'/actualiteDetail-' . $une[$i]->idActualite . '.php'); ?>" class="btn btn-primary btn-sm px-4 py-2 rounded-0">Voir plus</a></p>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="site-blocks-vs site-section bg-light">
      <div class="container">

        <div class="border mb-3 rounded d-block d-lg-flex align-items-center p-3 next-match">

          <div class="mr-auto order-md-1 w-60 text-center text-lg-left mb-3 mb-lg-0">
            <h3 class="h6 text-uppercase text-black font-weight-bold mb-3">Match suivant</h3>
            <div id="date-countdown"></div>
            <input type="hidden" id="dates" value="<?php echo $tete[0]->daty; ?>">
          </div>

          <div class="ml-auto pr-4 order-md-2">
            <div class="h5 text-black text-uppercase text-center text-lg-left">

            </div>
          </div>


        </div>

        <div class="bg-image overlay-success rounded mb-5" style="background-image: url(<?php echo  base_url('/images/local.jpg'); ?>);" data-stellar-background-ratio="0.5">

          <div class="row align-items-center">
            <div class="col-md-12 col-lg-4 mb-4 mb-lg-0">

              <div class="text-center text-lg-left">
                <div class="d-block d-lg-flex align-items-center">
                  <div class="image mx-auto mb-3 mb-lg-0 mr-lg-3">

                    <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $tete[0]->domiciles; ?>" alt="<?php echo $tete[0]->domicil; ?>" class="img-fluid">
                  </div>
                  <div class="text">
                    <h3 class="h5 mb-0 text-black"><?php echo $tete[0]->domicil; ?></h3>
                    <span class="text-uppercase small country text-black"><?php echo $tete[0]->region1; ?></span>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-12 col-lg-4 text-center mb-4 mb-lg-0">
              <div class="d-inline-block">
                <p class="mb-2"><small class="text-uppercase text-black font-weight-bold"><?php echo $tete[0]->libele; ?></small></p>
                <div class="bg-black py-2 px-4 mb-2 text-white d-inline-block rounded"><span class="h3">-</span></div>
                <p class="mb-0" id="complet"><small class="text-uppercase text-black font-weight-bold"><?php echo $tete[0]->daty; ?></small></p>
              </div>
            </div>

            <div class="col-md-12 col-lg-4 text-center text-lg-right">
              <div class="">
                <div class="d-block d-lg-flex align-items-center">
                  <div class="image mx-auto ml-lg-3 mb-3 mb-lg-0 order-2">
                    <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $tete[0]->exterieurs; ?>" alt="<?php echo $tete[0]->exterieure; ?>" class="img-fluid">
                  </div>
                  <div class="text order-1">
                    <h3 class="h5 mb-0 text-black"><?php echo $tete[0]->exterieure; ?></h3>
                    <span class="text-uppercase small country text-black"><?php echo $tete[0]->region2; ?></span>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-md-12">

            <h3 class="h6 text-uppercase text-black font-weight-bold mb-3">Résultats</h3>
            <div class="site-block-tab">
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                  <div class="row align-items-center">
                    <div class="col-md-12">

                      <?php for ($i = 0; $i < count($result); $i++) { ?>
                        <div class="row bg-white align-items-center ml-0 mr-0 py-4 match-entry">
                          <div class="col-md-4 col-lg-4 mb-4 mb-lg-0">

                            <div class="text-center text-lg-left">
                              <div class="d-block d-lg-flex align-items-center">
                                <div class="image image-small text-center mb-3 mb-lg-0 mr-lg-3">

                                  <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $result[$i]->domiciles; ?>" alt="<?php echo $result[$i]->domicil; ?>" class="img-fluid">
                                </div>
                                <div class="text">
                                  <h3 class="h5 mb-0 text-black"><?php echo $result[$i]->domicil; ?></h3>
                                  <span class="text-uppercase small country"><?php echo $result[$i]->region1; ?></span>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div class="col-md-4 col-lg-4 text-center mb-4 mb-lg-0">
                            <div class="d-inline-block">
                              <p><?php echo $result[$i]->libele; ?></p>
                              <div class="bg-black py-2 px-4 mb-2 text-white d-inline-block rounded"><span class="h5"><?php echo $result[$i]->scoreA . ":" . $result[$i]->scoreB; ?></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 text-center text-lg-right">
                            <div class="">
                              <div class="d-block d-lg-flex align-items-center">
                                <div class="image image-small ml-lg-3 mb-3 mb-lg-0 order-2">
                                  <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $result[$i]->exterieurs; ?>" alt="<?php echo $result[$i]->exterieure; ?>" class="img-fluid">
                                </div>
                                <div class="text order-1 w-100">
                                  <h3 class="h5 mb-0 text-black"><?php echo $result[$i]->exterieure; ?></h3>
                                  <span class="text-uppercase small country"><?php echo $result[$i]->region2; ?></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php } ?>

                    </div>

                  </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="row align-items-center">
                    <div class="col-md-12">


                      <div class="row bg-white align-items-center ml-0 mr-0 py-4 match-entry">
                        <div class="col-md-4 col-lg-4 mb-4 mb-lg-0">

                          <div class="text-center text-lg-left">
                            <div class="d-block d-lg-flex align-items-center">
                              <div class="image image-small text-center mb-3 mb-lg-0 mr-lg-3">
                                <img src="<?php echo  base_url('/images/img_1_sq.jpg'); ?>" alt="Image" class="img-fluid">
                              </div>
                              <div class="text">
                                <h3 class="h5 mb-0 text-black">Packers</h3>
                                <span class="text-uppercase small country">Brazil</span>
                              </div>
                            </div>
                          </div>

                        </div>
                        <div class="col-md-4 col-lg-4 text-center mb-4 mb-lg-0">
                          <div class="d-inline-block">
                            <div class="bg-black py-2 px-4 mb-2 text-white d-inline-block rounded"><span class="h5">3:2</span></div>
                          </div>
                        </div>

                        <div class="col-md-4 col-lg-4 text-center text-lg-right">
                          <div class="">
                            <div class="d-block d-lg-flex align-items-center">
                              <div class="image image-small ml-lg-3 mb-3 mb-lg-0 order-2">
                                <img src="<?php echo  base_url('/images/img_4_sq.jpg'); ?>" alt="Image" class="img-fluid">
                              </div>
                              <div class="text order-1 w-100">
                                <h3 class="h5 mb-0 text-black">Steelers</h3>
                                <span class="text-uppercase small country">London</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4">About Sportz</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
            </div>

            <div class="mb-5">
              <h3 class="footer-heading mb-4">Recent Blog</h3>
              <div class="block-25">
                <ul class="list-unstyled">
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="<?php echo base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="<?php echo base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="<?php echo base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Quick Menu</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Matches</a></li>
                  <li><a href="#">News</a></li>
                  <li><a href="#">Team</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Membership</a></li>
                </ul>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Follow Us</h3>

                <div>
                  <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                </div>
              </div>
            </div>

          </div>

          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="mb-5">
              <h3 class="footer-heading mb-4">Watch Video</h3>

              <div class="block-16">
                <figure>
                  <img src="<?php echo base_url('/images/img_1.jpg'); ?>" alt="Image placeholder" class="img-fluid rounded">
                  <a href="https://vimeo.com/channels/staffpicks/93951774" class="play-button popup-vimeo"><span class="icon-play"></span></a>
                </figure>
              </div>

            </div>

            <div class="mb-5">
              <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

              <form action="#" method="post">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="button-addon2">Send</button>
                  </div>
                </div>
              </form>

            </div>

          </div>

        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
              <script>
                document.write(new Date().getFullYear());
              </script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>

        </div>
      </div>
    </footer>
  </div>

  <script src="<?php echo base_url('/assets/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery-ui.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/owl.carousel.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.stellar.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.countdown.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.magnific-popup.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/aos.js'); ?>"></script>

  <script src="<?php echo base_url('/assets/js/main.js'); ?>"></script>


</body>

</html>