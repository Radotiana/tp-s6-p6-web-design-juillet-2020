<!DOCTYPE html>
<html lang="en">

<head>
    <title>GasyFou't - Classement sur toutes les championnats</title>
    <meta charset="utf-8">
    <meta name="description" content="Vous pouvez voir ci-dessous tous les résultats des matchs, toute championnat confondue, avec toutes les catégories">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo base_url('/assets/fonts/icomoon/style.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/magnific-popup.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/jquery-ui.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/assets/style.css'); ?>">


    <link rel="stylesheet" href="<?php echo base_url('/assets/css/aos.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('/assets/css/style.css'); ?>">

</head>

<body>

    <div class="site-wrap">

        <div class="site-mobile-menu">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-logo">
                    <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="https://res.cloudinary.com/gasyfout/image/upload/v1596577276/logo_sh7yzx.png" alt="logo"></a>
                </div>
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>

        <header class="site-navbar absolute transparent" role="banner">
            <nav class="site-navigation position-relative text-right bg-black text-md-right" role="navigation">
                <div class="container position-relative">
                    <div class="site-logo">
                        <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="https://res.cloudinary.com/gasyfout/image/upload/v1596577276/logo_sh7yzx.png" alt="logo"></a>
                    </div>

                    <div class="d-inline-block d-md-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

                    <ul class="site-menu js-clone-nav d-none d-md-block">
                        <li>
                            <h2><a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>">Accueil</a></h2>
                        </li>
                        <li>
                            <h2><a href="<?php echo base_url('GasyFou\'t - Actualités du foot Malagasy/actualite.php'); ?>">Actualités</a></h2>
                        </li>
                        <li>
                            <h2><a href="<?php echo base_url('GasyFou\'t - Les matchs à venir - Calendrier/match-1.php'); ?>">Matchs</a></h2>
                        </li>
                        <li>
                            <h2><a href="<?php echo base_url('GasyFou\'t - BAREA de Madagascar/equipe.php'); ?>">Equipes</a></h2>
                        </li>
                        <li class="active">
                            <h2><a href="#">Classement</a></h2>
                        </li>
                        <li>
                            <h2><a href="<?php echo base_url('GasyFou \'t - Résultats, Scores/resultat-1.php'); ?>">Résultats</a></h2>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="site-blocks-cover overlay" style="background-image: url(https://res.cloudinary.com/gasyfout/image/upload/v1596575816/about_1_dpf1dk.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-start">
                    <div class="col-md-6 text-center text-md-left" data-aos="fade-up" data-aos-delay="400">
                        <h1 class="bg-text-line">Classement sur toutes les championnats</h1>
                        <p class="mt-4">Vous pouvez voir ci-dessous tous les résultats des matchs, tout championnat confondu, avec toutes les catégories</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="site-section site-blocks-vs">
            <div class="container">
                <div style="text-align: center">
                    <form action="<?php echo base_url('GasyFou\'t - Classement sur toutes les championnats/classement.php'); ?>" method="post">
                        <h2>Veuillez choisir un championnat et une catégorie</h2>
                        <br>
                        <label style="font-weight: bold">Championnat</label>&nbsp&nbsp
                        <select name="championnat">
                            <?php for ($i = 0; $i < count($championnat); $i++) { ?>
                                <option value="<?php echo $championnat[$i]->idCompetition; ?>"><?php echo $championnat[$i]->libele; ?></option>
                            <?php } ?>
                        </select>
                        <br><br>
                        <label style="font-weight: bold">Categorie</label>&nbsp&nbsp
                        <select name="categorie">
                            <?php for ($i = 0; $i < count($categorie); $i++) { ?>
                                <option value="<?php echo $categorie[$i]->idCategorie; ?>"><?php echo $categorie[$i]->libele; ?></option>
                            <?php } ?>
                        </select>
                        <br><br>
                        <input type="submit" value="Valider">
                    </form>
                    <br>
                    <h2>Classement</h2>
                    <br>
                    <table style="width:50%; margin-left: 290px">
                        <tr>
                            <th>Equipe</th>
                            <th>Nombre match</th>
                            <th>Point</th>
                            <th>Différence de but</th>
                        </tr>
                        <?php for ($i = 0; $i < count($classement); $i++) { ?>
                            <tr>
                                <td><?php echo $classement[$i]->nom; ?></td>
                                <td style="text-align: right;"><?php echo $classement[$i]->nbmatch; ?></td>
                                <td style="text-align: right;"><?php echo $classement[$i]->points; ?></td>
                                <td style="text-align: right;"><?php echo $classement[$i]->goalaverage; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>

        <footer class="site-footer border-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-5">
                            <h3 class="footer-heading mb-4">About Sportz</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
                        </div>

                        <div class="mb-5">
                            <h3 class="footer-heading mb-4">Recent Blog</h3>
                            <div class="block-25">
                                <ul class="list-unstyled">
                                    <li class="mb-3">
                                        <a href="#" class="d-flex">
                                            <figure class="image mr-4">
                                                <img src="<?php echo base_url('/assets/images/img_1.jpg'); ?>" alt="img_1" class="img-fluid">
                                            </figure>
                                            <div class="text">
                                                <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="mb-3">
                                        <a href="#" class="d-flex">
                                            <figure class="image mr-4">
                                                <img src="<?php echo base_url('/assets/images/img_1.jpg'); ?>" alt="img_1" class="img-fluid">
                                            </figure>
                                            <div class="text">
                                                <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="mb-3">
                                        <a href="#" class="d-flex">
                                            <figure class="image mr-4">
                                                <img src="<?php echo base_url('/assets/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                                            </figure>
                                            <div class="text">
                                                <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="row mb-5">
                            <div class="col-md-12">
                                <h3 class="footer-heading mb-4">Quick Menu</h3>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Matches</a></li>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">Team</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Membership</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="footer-heading mb-4">Follow Us</h3>

                                <div>
                                    <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="mb-5">
                            <h3 class="footer-heading mb-4">Watch Video</h3>

                            <div class="block-16">
                                <figure>
                                    <img src="<?php echo base_url('/assets/images/img_1.jpg'); ?>" alt="Image placeholder" class="img-fluid rounded">
                                    <a href="https://vimeo.com/channels/staffpicks/93951774" class="play-button popup-vimeo"><span class="icon-play"></span></a>
                                </figure>
                            </div>

                        </div>

                        <div class="mb-5">
                            <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

                            <form action="#" method="post">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="button-addon2">Send</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
                <div class="row pt-5 mt-5 text-center">
                    <div class="col-md-12">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                            <script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </div>
        </footer>
    </div>

    <script src="<?php echo base_url('/assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery-ui.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/js/jquery.stellar.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery.countdown.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/aos.js'); ?>"></script>

    <script src="<?php echo base_url('/assets/js/main.js'); ?>"></script>
</body>

</html>