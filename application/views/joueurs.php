<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/brands.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/solid.css">
    <meta name="description" content="Ajout des joueurs">
    <title>GasyFou't - Joueurs</title>
</head>

<body id="log">
    <div>
        <h1 style="text-align: center">Joueurs</h1>
        <div style="margin-left: 30px; width:1500px">
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutNouvelle.php'); ?>"><button id="menu"><i class="fas fa-bars"></i>&nbsp Actualité</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutMatch.php'); ?>"><button id="menu"><i class="fas fa-calendar-alt"></i>&nbsp Matchs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutEquipe.php'); ?>"><button id="menu"><i class="fas fa-futbol"></i>&nbsp Equipes</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutJoueur.php'); ?>"><button id="menu"><i class="fas fa-user"></i>&nbsp Joueurs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutChampionnat.php'); ?>"><button id="menu"><i class="fas fa-medal"></i>&nbsp Championnat</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/deconnexion.php'); ?>"><button id="menu"><i class="fas fa-sign-out-alt"></i>&nbsp Deconnexion</button></a>
            </div>
        </div>

        <br><br><br><br>
        <div style="text-align: center;">
            <h2>Ajouter un joueurs</h2>
                <?php echo form_open_multipart('administrateur/insererJoueur.php'); ?>
                <label for="equipe">Equipe</label>
                <select name="equipe">
                    <?php for ($i = 0; $i < count($equipe); $i++) { ?>
                        <option value="<?php echo $equipe[$i]->idEquipe; ?>"><?php echo $equipe[$i]->nom; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="cat">Catégorie</label>
                <select name="categorie">
                    <?php for ($i = 0; $i < count($categorie); $i++) { ?>
                        <option value="<?php echo $categorie[$i]->idCategorie; ?>"><?php echo $categorie[$i]->libele; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="place">Place</label>
                <select name="place">
                    <?php for ($i = 0; $i < count($place); $i++) { ?>
                        <option value="<?php echo $place[$i]->idPlace; ?>"><?php echo $place[$i]->libele; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="nom">Nom</label>
                <input type="text" name="nom">
                <br><br>
                <label for="prenom">Prénom</label>
                <input type="text" name="prenom">
                <br><br>
                <label for="nb">Numero</label>
                <input type="number" name="numero">
                <br><br>
                <label for="logo">Photo</label>
                <input type="file" name="userfile" size="20" />
                <br /><br />
                <input type="submit" value="Ajouter">
                </form>
        </div>
    </div>
</body>

</html>