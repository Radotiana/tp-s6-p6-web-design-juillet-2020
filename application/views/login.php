<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
</head>

<body id="log">
    <br>
    <div class="login-wrap">
        <div class="login-html">
            <h1 style="color: white">GasyFou't BackOffice</h1>
            <br><br>
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label style="text-align: center" for="tab-1" class="tab">Connexion</label>
            <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
            <div class="login-form">
                <form action="<?php echo base_url('/administrateur/accueil.php'); ?>" method="post">
                    <div class="sign-in-htm">
                        <div class="group">
                            <label for="nom" style="color: #6a6f8c" class="label">E-mail</label>
                            <input name="login" id="nom" type="text" class="input">
                        </div>
                        <div class="group">
                            <label for="password" style="color: #6a6f8c" class="label">Mot de passe</label>
                            <input name="pwd" id="pass" type="password" class="input" data-type="password">
                            <?php
                            if ($error != null) { ?>
                                <script>
                                    alert("<?php echo $error; ?>");
                                </script>
                            <?php } ?>
                        </div>
                        <div class="group">
                            <input type="submit" style="color: white" class="button" value="Se connecter">
                            <br><br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>