<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultat</title>
</head>

<body>
    <form action="<?php echo base_url('administrateur/finir.php') ?>" method="post">
        <h1>Résultat du match</h1>
        <input type="hidden" value="<?php echo $idCompetition; ?>" name="idCompetition">
        <input type="hidden" value="<?php echo $idCategorie; ?>" name="idCategorie">
        <input type="hidden" value="<?php echo $domicile; ?>" name="domicile">
        <input type="hidden" value="<?php echo $idMatch; ?>" name="idMatch">
        <input type="hidden" value="<?php echo $exterieur; ?>" name="exterieur">
        <input type="hidden" value="<?php echo $exterieur; ?>" name="exterieur">
        <input type="hidden" name="daty" value="<?php echo $daty; ?>">
        <label>Equipe A: <?php echo $equipe[0]->domicil; ?></label>
        <br><br>
        <label>Score:</label>
        <input type="number" name="scoreA">
        <br><br>
        <label>Equipe B: <?php echo $equipe[0]->exterieure; ?></label>
        <br><br>
        <label>Score:</label>
        <input type="number" name="scoreB">
        <br><br>
        <input type="submit" value="Valider">
    </form>
</body>

</html>