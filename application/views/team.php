<?php
$gardien = array();
$defense = array();
$milieu = array();
$attaquant = array();
for ($i = 0; $i < count($joueur); $i++) {
  if ($joueur[$i]->idPlace == 1) {
    array_push($gardien, $joueur[$i]);
  } else if ($joueur[$i]->idPlace == 2) {
    array_push($defense, $joueur[$i]);
  } else if ($joueur[$i]->idPlace == 3) {
    array_push($milieu, $joueur[$i]);
  } else {
    array_push($attaquant, $joueur[$i]);
  }
}
// var_dump($joueur);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>GasyFou't - BAREA de Madagascar</title>
  <meta name="description" content="Voyons ci-dessous l'effectif de l'équipe national Malagasy de football">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="<?php echo base_url('/assets/fonts/icomoon/style.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/magnific-popup.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/jquery-ui.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.carousel.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/owl.theme.default.min.css'); ?>">


  <link rel="stylesheet" href="<?php echo base_url('/assets/css/aos.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/style.css'); ?>">

</head>

<body>

  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-logo">
          <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="<?php echo base_url('/images/logo.png'); ?>" alt="logo"></a>
        </div>
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar absolute transparent" role="banner">
      <nav class="site-navigation position-relative text-right bg-black text-md-right" role="navigation">
        <div class="container position-relative">
          <div class="site-logo">
            <a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>"><img src="<?php echo base_url('/images/logo.png'); ?>" alt="logo"></a>
          </div>

          <div class="d-inline-block d-md-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - le site web du foot Malagasy/accueil.php'); ?>">Accueil</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - Actualités du foot Malagasy/actualite.php'); ?>">Actualités</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou\'t - Les matchs à venir - Calendrier/match-1.php'); ?>">Matchs</a></h2>
            </li>
            <li class="active">
              <h2><a href="#">Equipes</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou \'t - Classement sur toutes les championnats/classement.php'); ?>">Classement</a></h2>
            </li>
            <li>
              <h2><a href="<?php echo base_url('GasyFou \'t - Résultats, Scores/resultat-1.php'); ?>">Résultats</a></h2>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <div class="site-blocks-cover overlay" style="background-image: url(https://res.cloudinary.com/gasyfout/image/upload/v1596575569/can_xbiz3m.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-start">
          <div class="col-md-6 text-center text-md-left" data-aos="fade-up" data-aos-delay="400">
            <h1 class="bg-text-line">Les BAREA</h1>
            <p class="mt-4">L'effectif de l'équipe national Malagasy de football</p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mb-5">
            <h2 class="text-black">Effectif</h2>
          </div>
        </div>
        <div class="row">
          <?php for ($m = 0; $m < count($gardien); $m++) { ?>
            <div class="mb-4 mb-lg-0 col-6 col-md-4 col-lg-2 text-center">
              <div class="player mb-5">
                <span class="team-number"><?php echo $gardien[$m]->numero; ?></span>

                <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $gardien[$m]->photo; ?>" alt="<?php echo $gardien[$m]->nom; ?>" class="img-fluid image rounded-circle">
                <h2><?php echo $gardien[$m]->prenom; ?></h2>
                <span class="position">Gardien de but</span>
              </div>
            </div>
          <?php
          } ?>
        </div>
        <div class="row">
          <?php for ($k = 0; $k < count($defense); $k++) { ?>
            <div class="mb-4 mb-lg-0 col-6 col-md-4 col-lg-2 text-center">
              <div class="player mb-5">
                <span class="team-number"><?php echo $defense[$k]->numero; ?></span>
                <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $defense[$k]->photo; ?>" alt="<?php echo $defense[$k]->nom; ?>" class="img-fluid image rounded-circle">
                <h2><?php echo $defense[$k]->prenom; ?></h2>
                <span class="position">Defenseur</span>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="row">
          <?php for ($l = 0; $l < count($milieu); $l++) { ?>
            <div class="mb-4 mb-lg-0 col-6 col-md-4 col-lg-2 text-center">
              <div class="player mb-5">
                <span class="team-number"><?php echo $milieu[$l]->numero; ?></span>
                <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $milieu[$l]->photo; ?>" alt="<?php echo $milieu[$l]->nom; ?>" class="img-fluid image rounded-circle">
                <h2><?php echo $milieu[$l]->prenom; ?></h2>
                <span class="position">Milieu</span>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="row">
          <?php for ($j = 0; $j < count($attaquant); $j++) { ?>
            <div class="mb-4 mb-lg-0 col-6 col-md-4 col-lg-2 text-center">
              <div class="player mb-5">
                <span class="team-number"><?php echo $attaquant[$j]->numero; ?></span>
                <img src="https://res.cloudinary.com/gasyfout/image/upload/v1596489287/<?php echo $attaquant[$j]->photo; ?>" alt="<?php echo $attaquant[$j]->nom; ?>" class="img-fluid image rounded-circle">
                <h2><?php echo $attaquant[$j]->prenom; ?></h2>
                <span class="position">Attaquant</span>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>



  <footer class="site-footer border-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="mb-5">
            <h3 class="footer-heading mb-4">About Sportz</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
          </div>

          <div class="mb-5">
            <h3 class="footer-heading mb-4">Recent Blog</h3>
            <div class="block-25">
              <ul class="list-unstyled">
                <li class="mb-3">
                  <a href="#" class="d-flex">
                    <figure class="image mr-4">
                      <img src="<?php echo  base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                    </figure>
                    <div class="text">
                      <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                    </div>
                  </a>
                </li>
                <li class="mb-3">
                  <a href="#" class="d-flex">
                    <figure class="image mr-4">
                      <img src="<?php echo  base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                    </figure>
                    <div class="text">
                      <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                    </div>
                  </a>
                </li>
                <li class="mb-3">
                  <a href="#" class="d-flex">
                    <figure class="image mr-4">
                      <img src="<?php echo  base_url('/images/img_1.jpg'); ?>" alt="" class="img-fluid">
                    </figure>
                    <div class="text">
                      <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>

        </div>
        <div class="col-lg-4 mb-5 mb-lg-0">
          <div class="row mb-5">
            <div class="col-md-12">
              <h3 class="footer-heading mb-4">Quick Menu</h3>
            </div>
            <div class="col-md-6 col-lg-6">
              <ul class="list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="#">Matches</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Team</a></li>
              </ul>
            </div>
            <div class="col-md-6 col-lg-6">
              <ul class="list-unstyled">
                <li><a href="#">About Us</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Membership</a></li>
              </ul>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <h3 class="footer-heading mb-4">Follow Us</h3>

              <div>
                <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-4 mb-5 mb-lg-0">
          <div class="mb-5">
            <h3 class="footer-heading mb-4">Watch Video</h3>

            <div class="block-16">
              <figure>
                <img src="<?php echo  base_url('/images/img_1.jpg'); ?>" alt="Image placeholder" class="img-fluid rounded">
                <a href="https://vimeo.com/channels/staffpicks/93951774" class="play-button popup-vimeo"><span class="icon-play"></span></a>
              </figure>
            </div>

          </div>

          <div class="mb-5">
            <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

            <form action="#" method="post">
              <div class="input-group mb-3">
                <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button" id="button-addon2">Send</button>
                </div>
              </div>
            </form>

          </div>

        </div>

      </div>
      <div class="row pt-5 mt-5 text-center">
        <div class="col-md-12">
          <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            <script>
              document.write(new Date().getFullYear());
            </script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>

      </div>
    </div>
  </footer>
  </div>

  <script src="<?php echo base_url('/assets/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery-ui.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/owl.carousel.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/js/jquery.stellar.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.countdown.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/jquery.magnific-popup.min.js'); ?>"></script>
  <script src="<?php echo base_url('/assets/js/aos.js'); ?>"></script>

  <script src="<?php echo base_url('/assets/js/main.js'); ?>"></script>

</body>

</html>