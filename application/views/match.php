<?php
if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/brands.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/solid.css">
    <meta name="description" content="Ajout et liste des matchs">
    <title>GasyFou't - Matchs</title>
</head>

<body id="log">
    <div>
        <h1 style="text-align: center">Matchs</h1>
        <div style="margin-left: 30px; width:1500px">
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutNouvelle.php'); ?>"><button id="menu"><i class="fas fa-bars"></i>&nbsp Actualité</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutMatch.php'); ?>"><button id="menu"><i class="fas fa-calendar-alt"></i>&nbsp Matchs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutEquipe.php'); ?>"><button id="menu"><i class="fas fa-futbol"></i>&nbsp Equipes</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutJoueur.php'); ?>"><button id="menu"><i class="fas fa-user"></i>&nbsp Joueurs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutChampionnat.php'); ?>"><button id="menu"><i class="fas fa-medal"></i>&nbsp Championnat</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/deconnexion.php'); ?>"><button id="menu"><i class="fas fa-sign-out-alt"></i>&nbsp Deconnexion</button></a>
            </div>
        </div>

        <br><br><br><br>
        <div style="text-align: center;">
            <h2>Ajouter une match</h2>
            <form action="<?php echo base_url('/administrateur/insererMatch.php'); ?>" method="post">
                <label for="cat">Compétitions</label>
                <select name="competition">
                    <?php for ($i = 0; $i < count($competition); $i++) { ?>
                        <option value="<?php echo $competition[$i]->idCompetition; ?>"><?php echo $competition[$i]->libele; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="cat">Catégorie</label>
                <select name="categorie">
                    <?php for ($i = 0; $i < count($categorie); $i++) { ?>
                        <option value="<?php echo $categorie[$i]->idCategorie; ?>"><?php echo $categorie[$i]->libele; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="stade">Date</label>
                <input type="text" name="date" placeholder="yyyy-mm-dd hh:mm:ss">
                <br><br>
                <label for="stade">Stade</label>
                <input type="text" name="stade">
                <br><br>
                <label for="cat">Equipe A</label>
                <select name="equipeA">
                    <?php for ($i = 0; $i < count($equipes); $i++) { ?>
                        <option value="<?php echo $equipes[$i]->idEquipe; ?>"><?php echo $equipes[$i]->nom; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <label for="cat">Equipe B</label>
                <select name="equipeB">
                    <?php for ($i = 0; $i < count($equipes); $i++) { ?>
                        <option value="<?php echo $equipes[$i]->idEquipe; ?>"><?php echo $equipes[$i]->nom; ?></option>
                    <?php } ?>
                </select>
                <br><br>
                <input type="submit" value="Ajouter">
            </form>
            <h2>Liste des matchs</h2>
            <table border="1" style="width:50%; margin-left: 390px">
                <tr>
                    <th>Competition</th>
                    <th>Date</th>
                    <th>Stade</th>
                    <th>Domicile</th>
                    <th>Exterieur</th>
                    <th>Finir</th>
                    <th>Supprimer</th>
                </tr>
                <?php for ($i = 0; $i < count($matchs); $i++) { ?>
                    <tr>
                        <td><?php echo $matchs[$i]->libele; ?></td>
                        <td><?php echo $matchs[$i]->daty; ?></td>
                        <td><?php echo $matchs[$i]->stade; ?></td>
                        <td><?php echo $matchs[$i]->domicil; ?></td>
                        <td><?php echo $matchs[$i]->exterieure; ?></td>
                        <td><a href="<?php echo base_url('administrateur/score-' . $matchs[$i]->idMatch . '-' . $matchs[$i]->idCompetition . '-' . $matchs[$i]->idCategorie . '-' . $matchs[$i]->domicile . '-' . $matchs[$i]->exterieur . '.php'); ?>">Fin</a></td>
                        <td><a href="<?php echo base_url('administrateur/supprimerMatch-'.$matchs[$i]->idMatch.'.php');?>">Supprimer</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</body>

</html>