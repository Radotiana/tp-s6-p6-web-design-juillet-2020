<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/brands.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/solid.css">
    <meta name="description" content="Ajout et liste des actualités">
    <title>GasyFou't - Actualité</title>
</head>

<body id="log">
    <div>
        <h1 style="text-align: center">Actualité</h1>
        <div style="margin-left: 30px; width:1500px">
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutNouvelle.php'); ?>"><button id="menu"><i class="fas fa-bars"></i>&nbsp Actualité</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutMatch.php'); ?>"><button id="menu"><i class="fas fa-calendar-alt"></i>&nbsp Matchs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutEquipe.php'); ?>"><button id="menu"><i class="fas fa-futbol"></i>&nbsp Equipes</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutJoueur.php'); ?>"><button id="menu"><i class="fas fa-user"></i>&nbsp Joueurs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutChampionnat.php'); ?>"><button id="menu"><i class="fas fa-medal"></i>&nbsp Championnat</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/deconnexion.php'); ?>"><button id="menu"><i class="fas fa-sign-out-alt"></i>&nbsp Deconnexion</button></a>
            </div>
        </div>

        <br><br><br><br>
        <div style="text-align: center;">
            <h2>Ajouter une actualité</h2>
            <?php echo form_open_multipart('administrateur/insererNouvelle.php'); ?>
            <label for="titre">Titre</label>
            <input type="text" name="titre">
            <br><br>
            <label for="photo">Ajouter une photo</label>
            <input type="file" name="userfile" size="20" />
            <br /><br />
            <label for="descri">Description</label>
            <textarea name="descri" style="width: 571px;height: 99px;"></textarea>
            <br><br>
            <input type="submit" values="Valider">
            </form>
            <h2>Liste des atualités</h2>
            <table border="1" style="width:95%;margin-left: 35px">
                <tr>
                    <th>Date</th>
                    <th>Titre</th>
                    <th>Photo</th>
                    <th>Description</th>
                    <th>Supprimer</th>
                </tr>
                <?php for ($i = 0; $i < count($news); $i++) { ?>
                    <tr>
                        <td><?php echo $news[$i]->daty; ?></td>
                        <td><?php echo $news[$i]->titre; ?></td>
                        <td><?php echo $news[$i]->photo; ?></td>
                        <td><?php echo $news[$i]->description; ?></td>
                        <td><a href="<?php echo base_url('administrateur/supprimerNouvelle-'.$news[$i]->idActualite.'.php');?>">Supprimer</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</body>

</html>