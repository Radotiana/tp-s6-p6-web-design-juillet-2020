<?php
if ($error != null) { ?>
    <script>
        alert("<?php echo $error; ?>");
    </script>
<?php } ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/brands.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/solid.css">
    <meta name="description" content="Ajout et liste des équipes">
    <title>GasyFou't - Equipes</title>
</head>

<body id="log">
    <div>
        <h1 style="text-align: center">Equipe</h1>
        <div style="margin-left: 30px; width:1500px">
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutNouvelle.php'); ?>"><button id="menu"><i class="fas fa-bars"></i>&nbsp Actualité</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutMatch.php'); ?>"><button id="menu"><i class="fas fa-calendar-alt"></i>&nbsp Matchs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutEquipe.php'); ?>"><button id="menu"><i class="fas fa-futbol"></i>&nbsp Equipes</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutJoueur.php'); ?>"><button id="menu"><i class="fas fa-user"></i>&nbsp Joueurs</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/ajoutChampionnat.php'); ?>"><button id="menu"><i class="fas fa-medal"></i>&nbsp Championnat</button></a>
            </div>
            <div style="float: left; margin-left: 30px">
                <a href="<?php echo base_url('/administrateur/deconnexion.php'); ?>"><button id="menu"><i class="fas fa-sign-out-alt"></i>&nbsp Deconnexion</button></a>
            </div>
        </div>

        <br><br><br><br>
        <div style="text-align: center;">
            <h2>Ajouter une équipe</h2>
            <?php echo form_open_multipart('administrateur/insererEquipe.php'); ?>
            <label for="cat">Catégorie</label>
            <select name="categorie">
                <?php for ($i = 0; $i < count($categorie); $i++) { ?>
                    <option value="<?php echo $categorie[$i]->idCategorie; ?>"><?php echo $categorie[$i]->libele; ?></option>
                <?php } ?>
            </select>
            <br><br>
            <label for="nom">Nom</label>
            <input type="text" name="nom">
            <br><br>
            <label for="logo">Ajouter un logo</label>
            <input type="file" name="userfile" size="20" />
            <br><br>
            <label for="region">Region</label>
            <input type="text" name="region">
            <br><br>
            <label for="pdt">President</label>
            <input type="text" name="president">
            <br><br>
            <label for="coach">Coach</label>
            <input type="coach" name="coach">
            <br><br>
            <input type="submit" value="Ajouter">
            </form>
            <h2>Liste des équipes</h2>
            <table border="1" style="width:50%; margin-left: 390px">
                <tr>
                    <th>Nom</th>
                    <th>Categorie</th>
                    <th>Logo</th>
                    <th>Région</th>
                    <th>Supprimer</th>
                </tr>
                <?php for ($i = 0; $i < count($equipes); $i++) { ?>
                    <tr>
                        <td><?php echo $equipes[$i]->nom; ?></td>
                        <td><?php echo $equipes[$i]->libele; ?></td>
                        <td><?php echo $equipes[$i]->logo; ?></td>
                        <td><?php echo $equipes[$i]->region; ?></td>
                        <td><a href="<?php echo base_url('administrateur/supprimerEquipe-'.$equipes[$i]->idEquipe.'.php');?>">Supprimer</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</body>

</html>