<?php
class UtilisateurController extends CI_Controller
{
    public function __construct()
    {
        // session_start();
        parent::__construct();
        $this->load->model('UtilisateurModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('cloudinarylib');
        // $this->load->library('session');
    }
    public function deconnecter()
    {
        redirect('administrateur/login.php');
    }
    public function nouvelle()
    {
        $data['news'] = $this->UtilisateurModel->listNews();
        $this->load->view('news', $data);
    }
    public function detail($idActualite)
    {
        $data['news'] = $this->UtilisateurModel->listNewsWhere($idActualite);
        $this->load->view('detail', $data);
    }
    public function classement()
    {
        $idCategorie = $this->input->post('categorie');
        $idChampionnat = $this->input->post('championnat');
        $cat = 1;
        $cha = 1;
        if ($idCategorie != null) {
            $cat = $idCategorie;
        }
        if ($idChampionnat != null) {
            $cha = $idChampionnat;
        }
        $classement = $this->UtilisateurModel->listClassement($cat, $cha);
        $categorie = $this->UtilisateurModel->listCategorie();
        $championnat = $this->UtilisateurModel->listCompetitions();
        $data['championnat'] = $championnat;
        $data['categorie'] = $categorie;
        $data['classement'] = $classement;
        $this->load->view('classement', $data);
    }
    public function listActualite()
    {
        $new = $this->UtilisateurModel->listNews();
        $tete = $this->UtilisateurModel->teteAffiche();
        $result = $this->UtilisateurModel->teteResultat();
        $une = $this->UtilisateurModel->aLaUne();
        $data['une'] = $une;
        $data['result'] = $result;
        $data['news'] = $new;
        $data['tete'] = $tete;
        $this->load->view('index', $data);
    }
    public function team()
    {
        $idJoueurs = 13;
        $joueur = $this->UtilisateurModel->listJoueurs($idJoueurs);
        $data['joueur'] = $joueur;
        $this->load->view('team', $data);
    }
    public function calendrier($nombre)
    {
        $nb = 1;
        if ($nombre != null) {
            $nb = $nombre;
        }
        $matches = $this->UtilisateurModel->listMatches($nb);
        $tete = $this->UtilisateurModel->teteAffiche();
        $data['tete'] = $tete;
        $data['matches'] = $matches;
        $this->load->view('matches', $data);
    }
    public function results($nombre)
    {
        $nb = 1;
        if ($nombre != null) {
            $nb = $nombre;
        }
        // echo $nb;
        $matches = $this->UtilisateurModel->listResultat($nb);
        $data['matches'] = $matches;
        $this->load->view('resultat', $data);
    }

    //BACK OFFICE
    //ACTUALITE
    public function news()
    {
        $data['news'] = $this->UtilisateurModel->listNews();
        $this->load->view('new', $data);
    }
    public function addNews()
    {
        $config['upload_path']          = './images';
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;
        $this->load->library('upload', $config);
        $nom = $_FILES['userfile']['name'];
        $chemin = $_FILES['userfile']['tmp_name'];
        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            // $this->load->view('equipe', $error);
            var_dump($error);
            // echo "non";
        } else {
            $dat['imageupload'] = \Cloudinary\Uploader::upload($chemin);
            $datas = array('upload_data' => $this->upload->data());
            $titre = $this->input->post('titre');
            $photo = $dat['imageupload']['public_id']; //$datas['upload_data']['file_name'];
            $daty = date('Y-m-d H:i:s');
            $description = $this->input->post('descri');
            $data = array(
                'daty' => $daty,
                'titre' => $titre,
                'photo' => $photo,
                'etat' => 0,
                'description' => $description
            );
            $this->UtilisateurModel->addNews($data);
            redirect('administrateur/ajoutNouvelle.php');
        }
    }
    //EQUIPE
    public function equipe()
    {
        $data['error']="";
        $data['categorie'] = $this->UtilisateurModel->listCategorie();
        $data['equipes'] = $this->UtilisateurModel->listEquipes();
        $this->load->view('equipe', $data);
    }
    public function addEquipes()
    {
        $config['upload_path']          = './images';
        $config['allowed_types']        = '*';
        $config['max_size']             = 100000000;
        $config['max_width']            = 50000000;
        $config['max_height']           = 50000000;
        $data = null;
        $nom = $_FILES['userfile']['name'];
        $chemin = $_FILES['userfile']['tmp_name'];
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            // echo "non";
            // echo is_dir($config["upload_path"]);
            // var_dump($config['upload_path']);
            $error = array('error' => $this->upload->display_errors());
            // $this->load->view('equipe', $error);
            var_dump($error);
        } else {
            $dat['imageupload'] = \Cloudinary\Uploader::upload($chemin);
            // var_dump($dat['imageupload']['public_id']);
            // echo "oui";
            $datas = array('upload_data' => $this->upload->data());
            $idCategorie = $this->input->post('categorie');
            $nom = $this->input->post('nom');
            $logo = $dat['imageupload']['public_id']; //$datas['upload_data']['file_name'];
            $region = $this->input->post('region');
            $president = $this->input->post('president');
            $coach = $this->input->post('coach');
            $data = array(
                'idCategorie' => $idCategorie,
                'nom' => $nom,
                'logo' => $logo,
                'region' => $region,
                'president' => $president,
                'coach' => $coach
            );
            var_dump($data);
            // $this->UtilisateurModel->addEquipes($idCategorie, $nom, $logo, $region, $president, $coach);
            $this->UtilisateurModel->addEquipes($data);
            redirect('administrateur/ajoutEquipe.php');
        }
    }


    //COMPETITION
    public function competition()
    {
        $data['categorie'] = $this->UtilisateurModel->listCategorie();
        $data['competitions'] = $this->UtilisateurModel->listCompetitions();
        $this->load->view('competition', $data);
    }
    public function addCompetitions()
    {
        $config['upload_path']          = './images';
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;
        $this->load->library('upload', $config);
        $nom = $_FILES['userfile']['name'];
        $chemin = $_FILES['userfile']['tmp_name'];

        if (!$this->upload->do_upload('userfile')) {
            // echo "non";
            $error = array('error' => $this->upload->display_errors());
            // $this->load->view('equipe', $error);
            var_dump($error);
        } else {
            // echo "oui";
            $dat['imageupload'] = \Cloudinary\Uploader::upload($chemin);

            $datas = array('upload_data' => $this->upload->data());
            $idCategorie = $this->input->post('categorie');
            $nom = $this->input->post('nom');
            $logo = $dat['imageupload']['public_id']; //$datas['upload_data']['file_name'];
            $nombre = $this->input->post('nombre');
            $saison = $this->input->post('saison');
            $data = array(
                'idCategorie' => $idCategorie,
                'libele' => $nom,
                'logo' => $logo,
                'nombreequipe' => $nombre,
                'saison' => $saison,
                'etat' => 0
            );
            $this->UtilisateurModel->addCompetition($data);
            redirect('administrateur/ajoutChampionnat.php');
        }
    }


    //MATCH
    public function match()
    {
        $data['error']="";
        $data['matchs'] = $this->UtilisateurModel->listMatchs();
        $data['competition'] = $this->UtilisateurModel->listCompetitions();
        $data['categorie'] = $this->UtilisateurModel->listCategorie();
        $data['equipes'] = $this->UtilisateurModel->listEquipes();
        $this->load->view('match', $data);
    }
    public function addMatchs()
    {
        $idCompetition = $this->input->post('competition');
        $stade = $this->input->post('stade');
        $daty = $this->input->post('date');
        $idCategorie = $this->input->post('categorie');
        $domicile = $this->input->post('equipeA');
        $exterieur = $this->input->post('equipeB');
        $data = array(
            'idCompetition' => $idCompetition,
            'idCategorie' =>  $idCategorie,
            'daty' => $daty,
            'stade' => $stade,
            'domicile' => $domicile,
            'scoreA' => 0,
            'exterieur' => $exterieur,
            'scoreB' => 0,
            'etat' => 0
        );
        $pointA = $this->UtilisateurModel->getPoints($domicile, $idCompetition, $idCategorie);
        $pointB = $this->UtilisateurModel->getPoints($exterieur, $idCompetition, $idCategorie);
        if ($pointA == null) {
            $dataA = array(
                'idCompetition' => $idCompetition,
                'idCategorie' =>  $idCategorie,
                'idEquipe' => $domicile,
                'points' => 0,
                'nbmatch' => 0,
                'goalaverage' => 0
            );
            $this->UtilisateurModel->addClassement($dataA);
        }
        if ($pointB == null) {
            $dataB = array(
                'idCompetition' => $idCompetition,
                'idCategorie' =>  $idCategorie,
                'idEquipe' => $exterieur,
                'points' => 0,
                'nbmatch' => 0,
                'goalaverage' => 0
            );
            $this->UtilisateurModel->addClassement($dataB);
        }
        $this->UtilisateurModel->addMatchs($data);
        redirect('administrateur/ajoutMatch.php');
    }
    public function fin($idMatch, $idCompetition, $idCategorie, $domicile, $exterieur)
    {
        // echo $idCategorie;
        $list = $this->UtilisateurModel->listMatchsWhere($idMatch);
        $data['equipe'] = $list;
        $data['idMatch'] = $idMatch;
        $data['idCompetition'] = $idCompetition;
        $data['idCategorie'] = $idCategorie;
        $data['domicile'] = $domicile;
        $data['exterieur'] = $exterieur;
        $data['daty'] = $list[0]->daty;
        // var_dump($data);
        $this->load->view('result', $data);
    }
    public function finir()
    {
        try{
            $idMatch = $this->input->post('idMatch');
            $idCompetition = $this->input->post('idCompetition');
            $idCategorie = $this->input->post('idCategorie');
            // echo $idCategorie;
            $domicile = $this->input->post('domicile');
            $exterieur = $this->input->post('exterieur');
            $scoreA = $this->input->post('scoreA');
            $scoreB = $this->input->post('scoreB');

            $daty = $this->input->post('daty');
            $now = date('Y-m-d H:i:s');
            if ($daty < $now) {
            $this->UtilisateurModel->calculPoint($idCompetition, $idCategorie, $domicile, $scoreA, $exterieur, $scoreB);
            $array = array(
                'scoreA' => $scoreA,
                'scoreB' => $scoreB,
                'etat' => 1
            );
            $this->UtilisateurModel->updateMatch($idMatch, $array);
            redirect('administrateur/ajoutMatch.php');
            } else {
                throw new Exception("La date du match n'est pas encore arrivé");
            }
        }
        catch(Exception $e)
        {
            $data['error'] = $e->getMessage();
            $data['matchs'] = $this->UtilisateurModel->listMatchs();
            $data['competition'] = $this->UtilisateurModel->listCompetitions();
            $data['categorie'] = $this->UtilisateurModel->listCategorie();
            $data['equipes'] = $this->UtilisateurModel->listEquipes();
            $this->load->view('match', $data);
        }
    }

    //JOUEURS
    public function joueurs()
    {
        $data['categorie'] = $this->UtilisateurModel->listCategorie();
        $data['equipe'] = $this->UtilisateurModel->listEquipes();
        $data['place'] = $this->UtilisateurModel->listPlaces();
        $this->load->view('joueurs', $data);
    }
    public function addJoueurs()
    {
        $config['upload_path']          = './images';
        $config['allowed_types']        = '*';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;
        $this->load->library('upload', $config);
        $nom = $_FILES['userfile']['name'];
        $chemin = $_FILES['userfile']['tmp_name'];

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
            // $this->load->view('equipe', $error);
            // echo "non";
        } else {
            $dat['imageupload'] = \Cloudinary\Uploader::upload($chemin);

            $datas = array('upload_data' => $this->upload->data());
            $idCategorie = $this->input->post('categorie');
            $equipe = $this->input->post('equipe');
            $place = $this->input->post('place');
            $nom = $this->input->post('nom');
            $photo = $dat['imageupload']['public_id']; //$datas['upload_data']['file_name'];
            $numero = $this->input->post('numero');
            $prenom = $this->input->post('prenom');
            $data = array(
                'idEquipe' => $equipe,
                'idCategorie' => $idCategorie,
                'idPlace' => $place,
                'nom' => $nom,
                'prenom' => $prenom,
                'numero' => $numero,
                'photo' => $photo
            );
            $this->UtilisateurModel->addJoueur($data);
            redirect('administrateur/ajoutJoueur.php');
        }
    }


    /*public function ajout()
    {
        $data['profils'] = $this->ProfilModel->listProfil();
        $this->load->view('ajoutUtilisateur', $data);
    }*/
    public function modification($idUtilisateur)
    {
        $data['idUtilisateur'] = $idUtilisateur;
        $response = $this->UtilisateurModel->listUtilisateurWhere($idUtilisateur);
        $data['nom'] = $response[0]->NOM;
        $data['pwd'] = $response[0]->PASSWORD;
        // $data['profil'] = $this->ProfilModel->listProfil();
        //var_dump($response);
        $this->load->view('modifierUtilisateur', $data);
    }
    public function logs()
    {
        $data['error'] = "";
        $this->load->view('login', $data);
    }
    public function login()
    {
        try {
            $nom = $this->input->post('login');
            $pwd = $this->input->post('pwd');
            $reponse = $this->UtilisateurModel->log($nom, $pwd);
            if ($reponse != null) {
                $this->load->view('accueil');
            } else {
                throw new Exception("Votre login ou mot de passe est incorrect");
            }
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
            $this->load->view('login', $data);
        }
    }
    public function traitementAjout()
    {
        $idProfil = $this->input->post('idProfil');
        $nom = $this->input->post('nom');
        $password = $this->input->post('pwd');
        //$this->load->library('password');
        $data = array(
            'idProfil' => $idProfil,
            'nom' => $nom,
            'password' => password_hash($password, PASSWORD_BCRYPT)
        );
        $this->UtilisateurModel->addUtilisateur($data);
        redirect('UtilisateurController/listUtilisateur');
        //echo $designation;
    }
    public function listUtilisateur()
    {
        $tabl = $this->UtilisateurModel->listUtilisateur();
        $data['users'] = $tabl;
        $this->load->view('utilisateur', $data);
    }
    public function deleteUtilisateurs($idUtilisateur)
    {
        $this->UtilisateurModel->deleteUtilisateur($idUtilisateur);
        redirect('UtilisateurController/listUtilisateur');
    }
    public function deleteActualite($idActualite)
    {
        $this->UtilisateurModel->deleteActualite($idActualite);
        redirect('administrateur/ajoutNouvelle.php');
    }
    public function deleteMatch($idMatch)
    {
        $this->UtilisateurModel->deleteMatch($idMatch);
        redirect('administrateur/ajoutMatch.php');
    }
    public function deleteCompetition($idCompetition)
    {
        $this->UtilisateurModel->deleteCompetition($idCompetition);
        redirect('administrateur/ajoutChampionnat.php');
    }
    public function deleteEquipe($idEquipe)
    {
        try{
        $this->UtilisateurModel->deleteEquipe($idEquipe);
        redirect('administrateur/ajoutEquipe.php');
        }
        catch(Exception $e)
        {
            $data['error']="";
            $data['categorie'] = $this->UtilisateurModel->listCategorie();
            $data['equipes'] = $this->UtilisateurModel->listEquipes();
            $data['error'] = $e->getMessage();
            $this->load->view('equipe',$data);
        }
    }
    public function updateUtilisateurs()
    {
        $idUtilisateur = $this->input->post('idUtilisateur');
        $nom = $this->input->post('nom');
        $password = $this->input->post('pwd');
        $idProfil = $this->input->post('idProfil');
        $data = array(
            'idProfil' => $idProfil,
            'nom' => $nom,
            'password' => password_hash($password, PASSWORD_BCRYPT)
        );
        var_dump($idUtilisateur);
        $this->UtilisateurModel->updateUtilisateur($idUtilisateur, $data);
        redirect('UtilisateurController/listUtilisateur');
    }
}
