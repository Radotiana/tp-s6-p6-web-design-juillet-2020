<?php

class Upload extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $this->load->view('upload_form', array('error' => ' '));
    }

    public function do_upload()
    {

        $config['upload_path']          = './assets/images';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            // $error = array('error' => $this->upload->display_errors());
            $data['rep'] = "an error was occured in upload";
            $this->load->view('upload_form', $data);
        } else {
            $datas = array('upload_data' => $this->upload->data());
            $data['rep'] = "upload successfully";
            $data['data'] = $datas;
            $this->load->view('upload_form', $data);
            //$this->load->view('upload_success', $data);
        }
    }
}
