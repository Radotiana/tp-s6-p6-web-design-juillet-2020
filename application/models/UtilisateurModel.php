<?php
class UtilisateurModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function listUtilisateur()
    {
        $user = $this->db->get('User');
        if ($user->num_rows() > 0) {
            return $user->result();
        } else {
            return array();
        }
    }
    public function log($nom, $pwd)
    {
        //$this->load->library('password');
        $reponses = null;
        //$pass = password_hash($pwd, PASSWORD_DEFAULT);
        $this->db->where('login', $nom);
        $this->db->where('pwd', $pwd);
        $utilisateur = $this->db->get('utilisateur');
        if ($utilisateur->num_rows() == 1) {
            $reponses = $utilisateur->result();
            /*if (password_verify($pwd, $reponse[0]->PASSWORD)) {
                $reponses = array(
                    'idProfil' => $reponse[0]->IDPROFIL,
                    'idUtilisateur' => $reponse[0]->IDUTILISATEUR
                );
            } else {
                throw new Exception('Votre nom ou mot de passe incorrect');
            }*/
        } else {
            throw new Exception('Votre nom ou mot de passe incorrect');
        }
        return $reponses;
    }
    public function aLaUne()
    {
        $query = $this->db->query("select * from actualite order by daty desc limit 3");
        return $query->result();
    }
    public function addNews($data)
    {
        $this->db->insert('actualite', $data);
    }
    public function listNews()
    {
        $this->db->order_by('daty', 'DESC');
        $user = $this->db->get('actualite');
        if ($user->num_rows() > 0) {
            return $user->result();
        } else {
            return array();
        }
    }
    public function listNewsWhere($idActualite)
    {
        $query = $this->db->query("Select * from actualite where idActualite=" . $idActualite);
        return $query->result();
    }

    /*public function addEquipes($idCategorie,$nom,$logo,$region,$president,$coach)
    {
        $this->db->set('idCategorie', $idCategorie);
        $this->db->set('nom', $nom);
        $this->db->set('logo', $logo);
        $this->db->set('region', $region);
        $this->db->set('president', $president);
        $this->db->set('coach', $coach);
		$this->db->insert('equipe');
    }*/
    public function getSequence($sequence)
    {
        $query = $this->db->query("select nextval('" . $sequence . "') AS " . $sequence);
        return $query->result()[0]->$sequence;
    }
    public function addEquipes($data)
    {
        $this->db->insert('equipe', $data);
    }
    /*public function addEquipes($idCategorie, $nom, $logo, $region, $president, $coach)
    {
        $idEquipe = $this->getSequence("idEquipe");
        $query = $this->db->query("insert into equipe values (" . $idEquipe . "," . $idCategorie . ",'" . $nom . "','" . $logo . "','" . $region . "','" . $president . "','" . $coach . "')");
    }*/
    public function listEquipes()
    {
        $query = $this->db->query("select equipe.*,categorie.libele from equipe join categorie on equipe.idCategorie=categorie.idCategorie");
        return $query->result();
    }
    public function listCategorie()
    {
        $query = $this->db->query("select * from categorie");
        return $query->result();
    }

    public function addMatchs($data)
    {
        $this->db->insert('matchs', $data);
    }
    public function teteAffiche()
    {
        $zao = date('Y-m-d H:i:s');
        $query = $this->db->query("Select m.*,e1.logo as domiciles,e2.logo as exterieurs,e1.region as region1, e2.region as region2,competition.libele,e1.nom as domicil,e2.nom as exterieure From matchs m join equipe e1 on m.domicile=e1.idEquipe join equipe e2 on m.exterieur=e2.idEquipe join competition on competition.idCompetition=m.idcompetition where daty>'" . $zao . "' order by daty asc limit 1");
        return $query->result();
    }
    public function teteResultat()
    {
        $query = $this->db->query("Select * from matche where etat=1 limit 2");
        return $query->result();
    }
    public function listResultat($debuts)
    {
        $debut = (($debuts - 1) * 3);
        $query = $this->db->query("Select * from matche where etat=1 order by daty desc limit " . $debut . ",3");
        return $query->result();
    }
    public function listMatchs()
    {
        $query = $this->db->query("Select * from matche where etat=0");
        return $query->result();
    }
    public function listMatches($debuts)
    {
        $debut = (($debuts - 1) * 3);
        $query = $this->db->query("Select * from matche where etat=0 order by daty asc limit " . $debut . ",3");
        return $query->result();
    }
    public function listMatchsWhere($idMatch)
    {
        $query = $this->db->query("Select * from matche where idMatch=" . $idMatch);
        return $query->result();
    }
    public function updateMatch($idMatch, $data)
    {
        var_dump($idMatch);
        $this->db->where('idMatch', $idMatch);
        $this->db->update('matchs', $data);
    }
    public function updateClassement($idCompetition, $idCategorie, $idEquipe, $data)
    {
        $this->db->where('idCompetition', $idCompetition);
        $this->db->where('idCategorie', $idCategorie);
        $this->db->where('idEquipe', $idEquipe);
        $this->db->update('classement', $data);
    }
    public function addClassement($data)
    {
        $this->db->insert('classement', $data);
    }
    public function listClassement($idCategorie, $idChampionnat)
    {
        $query = $this->db->query("Select classement.*,equipe.nom from classement join equipe ON classement.idEquipe=equipe.idEquipe where idCompetition=" . $idChampionnat . " and classement.idCategorie=" . $idCategorie . " order by points desc, nbmatch desc ,goalaverage desc");
        return $query->result();
    }
    public function getPoints($idEquipe, $idCompetition, $idCategorie)
    {
        $query = $this->db->query("Select points,goalaverage,nbmatch from classement where idEquipe=" . $idEquipe . " and idCompetition=" . $idCompetition . " and idCategorie=" . $idCategorie);
        return $query->result();
    }

    public function calculPoint($idCompetition, $idCategorie, $equipeA, $scoreA, $equipeB, $scoreB)
    {
        $pointsAs = $this->getPoints($equipeA, $idCompetition, $idCategorie);
        $pointsBs = $this->getPoints($equipeB, $idCompetition, $idCategorie);
        $pointsA = $pointsAs[0]->points;
        $pointsB = $pointsBs[0]->points;
        if ($scoreA > $scoreB) {
            $pointsA += 3;
        } else if ($scoreA < $scoreB) {
            $pointsB += 3;
        } else {
            $pointsA += 1;
            $pointsB += 1;
        }
        $diffA = $pointsAs[0]->goalaverage;
        $diffB = $pointsBs[0]->goalaverage;
        $A = $scoreA - $scoreB;
        $B = $scoreB - $scoreA;
        $diffA += $A;
        $diffB += $B;
        $nbA = $pointsAs[0]->nbmatch;
        $nbB = $pointsBs[0]->nbmatch;
        $nbA += 1;
        $nbB += 1;

        $dataA = array(
            'points' => $pointsA,
            'nbmatch' => $nbA,
            'goalaverage' => $diffA
        );
        $dataB = array(
            'points' => $pointsB,
            'nbmatch' => $nbB,
            'goalaverage' => $diffB
        );
        // var_dump($dataA);
        // var_dump($dataB);
        $this->updateClassement($idCompetition, $idCategorie, $equipeA, $dataA);
        $this->updateClassement($idCompetition, $idCategorie, $equipeB, $dataB);
    }

    public function addCompetition($data)
    {
        $this->db->insert('competition', $data);
    }
    public function listCompetitions()
    {
        $query = $this->db->query("select competition.*,categorie.libele as libeles from competition join categorie on competition.idCategorie=categorie.idCategorie");
        return $query->result();
    }

    public function listPlaces()
    {
        $query = $this->db->query("select * from place");
        return $query->result();
    }

    public function addJoueur($data)
    {
        $this->db->insert('joueur', $data);
    }
    public function listJoueurs($idEquipe)
    {
        $query = $this->db->query("select * from joueur where idEquipe=" . $idEquipe . " order by idPlace");
        return $query->result();
    }

    public function addUtilisateur($data)
    {
        $this->db->insert('utilisateur', $data);
    }
    public function deleteMatch($idMatch)
    {
        $this->db->delete('matchs', 'idMatch=' . $idMatch);
    }
    public function deleteEquipe($idEquipe)
    {
        $this->db->delete('equipe', 'idEquipe=' . $idEquipe);
    }
    public function deleteActualite($idActualite)
    {
        $this->db->delete('actualite', 'idActualite=' . $idActualite);
    }
    public function deleteCompetition($idCompetition)
    {
        $this->db->delete('competition', 'idCompetition=' . $idCompetition);
    }

    public function updateUtilisateur($idUtilisateur, $data)
    {
        //$this->db->set($data);
        var_dump($data);
        var_dump($idUtilisateur);
        $this->db->where('IDUTILISATEUR', $idUtilisateur);
        $this->db->update('utilisateur', $data);
    }
}
